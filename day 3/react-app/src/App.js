// import logo from './logo.svg';
import './App.css'
import {useState,useEffect} from 'react';

// function App() {
//   return (
//     <div>
//       <h1>HELLO FROM REACT</h1>
//       <Counter></Counter>
//     </div>
//   );

// }
 
// class Counter extends  Component{

//   state = {
//     count: 0,
//     date: Date().toString()
//   }
 

//   onClick() {
//     let newCount = this.state.count + 1;
//     this.setState({
//       count: newCount
//     })

//     if(newCount % 2 == 0){
//       this.setState({
//         date: Date().toLocaleTimeString()
//       })
  
//     }
//   }

//   render(){
//      return (
//        <div>
//          <p>Count {this.state.count}</p>
//          <p>{this.state.date}</p>
//          <button
//            onClick={() => {
//              this.setState({
//                count: this.state.count + 1,
//              });
//            }}
//          >
//            +
//          </button>
//          <button
//            onClick={() => {
//              this.setState({
//                count: this.state.count - 1,
//              });
//            }}
//          >
//            -
//          </button>

//         {this.state.count %2 == 0 &&   <Timer number={0} />}
//        </div>
//      );
//   }
// }

// class Timer extends  Component{
//   timer = null;
//   state = {
//     date: (new Date()).toLocaleTimeString(),
//     key:""
//   }

//   componentDidMount(){
//     this.timer= setInterval(() => {
//         this.setState({
//           date: (new Date()).toLocaleTimeString()
//         })
//     }, 1000);
//     console.log("Mount ",this.timer)
//   }
  
//   componentWillUnmount(){
//     // clearInterval(this.timer)
//     console.log("componentWillUnmount")
//   }
  
//   componentDidUpdate(){
//     console.log("componentDidUpdate");
//   }

//   render(){
//    return (
//      <div>
//        <p>{this.state.date}</p>
//        {this.props.number}
//      </div>
//    );
//   }
// }

function Profile(props){
  return (
    <div style={props.style}>
      <h3>Name: {props.name} </h3>
      <h4>Age : {props.age} </h4>
      {props.children}
    </div>
  )
}

// export default App;

function App() {

  const [count,setCount] = useState(0)
  const [message,setMessage] = useState("hello")
 
  return (
    <div>
      <p>count : {count} </p>
      <button
        type="button"
        onClick={() => {
          setCount(count + 1);
        }}
      >
        +
      </button>
       <button
        type="button"
        onClick={() => {
          setCount(count - 1);
        }}
      >
        -
      </button>
      <p>{message}</p>
      <button onClick={()=>{
        setMessage("Message changed")
      }}>Change message</button>

     {count== 5 &&  <Timer/>}
    </div>
  );
}


function Timer () {

  const [time , setTime] = useState((new Date()).toLocaleTimeString())
  const [key , setKey] = useState("")

  useEffect(()=>{
    
    console.log("component mounted")
    return ()=>{
      console.log("component un mounting...")
    }
  },[])

  useEffect(() => {
    console.log("changed", time);
  }, [time]);


  
  return <div>
    <p>{time}</p>
    <button onClick={()=>[
      setTime((new Date()).toLocaleTimeString())
    ]}>Change time</button>
  <p>  {key}</p>
  </div>
}

export default App