import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import Quiz from './components/quiz';
import Todo from './components/todo/todo';
import './index.css'
const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//     <App />
// );
// root.render(
//     <Quiz />
// );
root.render(
    <Todo />
);
