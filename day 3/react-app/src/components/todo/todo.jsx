
import { useState , createContext, useContext } from 'react';
import './todo.css'
const FilterContext =  createContext('todoApp');

const Todo = () => {
    const [todos,setTodo] = useState([])
    const [filter,setFilter] = useState(0)

    let filteredTodos = todos;
    if(filter == 1){
      filteredTodos = todos.filter(i=>!i.isChecked)
    }
    if(filter == 2){
      filteredTodos = todos.filter(i=>i.isChecked)
    }
    
    return (
      <FilterContext.Provider value={filter}>
        <section className="todoapp">
          <TodoHeader
            onNewTodo={(newTodo) => {
              setTodo([...todos, newTodo]);
            }}
          />
          <TodoBody
            todos={filteredTodos}
            onCheck={(id) => {
              setTodo([
                ...todos.map((element) => {
                  if (element.id == id) {
                    element.isChecked = !element.isChecked;
                  }
                  return element;
                }),
              ]);
            }}
            onDelete={(id) => {
              setTodo([...todos.filter((e) => e.id != id)]);
            }}
          />
          <TodoFooter
            filter={filter}
            itemsLett={todos.filter((i) => !i.isChecked).length}
            onFilterChange={(filter) => setFilter(filter)}
          />
        </section>
      </FilterContext.Provider>
    );
}



const TodoHeader = (props) => {

    const [inputValue,setInputValue] = useState("")
    return (
      <header className="header">
        <h1>todos</h1>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            const todo = {
              text: inputValue,
              isChecked: false,
              id: new Date().getTime(),
            };
            setInputValue("");
            props.onNewTodo(todo);
          }}
        >
          <input
            onChange={(e) => {
              console.log(e.target.value.indexOf("sarath"));
              if (e.target.value.indexOf("sarath") != -1) {
                setInputValue("");
              } else {
                setInputValue(e.target.value);
              }
            }}
            value={inputValue}
            className="new-todo"
            placeholder="What needs to be done?"
          />
        </form>
      </header>
    );
}
 

 
const TodoBody = (props) => {
    return (
      <section className="main" style={{"display":"block"}}>
        <input id="toggle-all" className="toggle-all" type="checkbox" />
        <label for="toggle-all">Mark all as complete</label>
        <ul className="todo-list">
            {props.todos.map(todo=>{

                    return (
                      <li
                        key={todo.id}
                        className={todo.isChecked ? "completed" : ""}
                      >
                        <div className="view">
                          <input
                            className="toggle"
                            type="checkbox"
                            onChange={() => {
                              props.onCheck(todo.id);
                            }}
                            checked=""
                          />
                          <label>{todo.text}</label>
                          <button
                            className="destroy"
                            onClick={() => props.onDelete(todo.id)}
                          ></button>
                        </div>
                      </li>
                    );

            })
            }
         
        </ul>
      </section>
    );
}
 
const  TodoFooter = ({itemsLett,onFilterChange,filter}) => {
    return (
      <footer className="footer" style={{ display: "block" }}>
        <span className="todo-count">
          <strong>{itemsLett}</strong> items left
        </span>
        <TodoFilter filter={filter} onFilterChange={onFilterChange} />
        <button className="clear-completed" style={{ display: "block" }}>
          Clear completed
        </button>
      </footer>
    );
}
  

const TodoFilter = ({onFilterChange}) => {
  const filter = useContext(FilterContext)
  
  return (
    <ul className="filters">
      <li>
        <a
          href="#/"
          onClick={() => {
            onFilterChange(0);
          }}
          className={filter == 0 ? "active" : ""}
        >
          All
        </a>
      </li>
      <li>
        <a
          href="#/active"
          onClick={() => {
            onFilterChange(1);
          }}
          className={filter == 1 ? "active" : ""}
        >
          Active
        </a>
      </li>
      <li>
        <a
          href="#/completed"
          onClick={() => {
            onFilterChange(2);
          }}
          className={filter == 2 ? "active" : ""}
        >
          Completed
        </a>
      </li>
    </ul>
  );
};
 
export default Todo;