console.log("app started")
let todos = []
const todoList = document.getElementById("todoList");
const input  = document.getElementById("todoInput");
const submit  = document.getElementById("todoSubmit");
 
submit.addEventListener("click",()=>{
    const todo = {
      text: input.value,
      isCompleted: false,
    };
    todos.push(todo);
    todoRender();
})

function todoRender(){
    todoList.innerHTML = "";
    todos.forEach((todo)=>{
       createLi(todo)
    })
}

function toggleCompelte(todo){
    todos =  todos.map(element=>{
        if(element.text == todo.text){
            element.isCompleted = !element.isCompleted
        }
        return element;
    })
    todoRender();
    console.log(todos);
}


function createLi(todo){
    const li = document.createElement("li")
    const span= document.createElement("span")
    const deleteButton = document.createElement("button")
    deleteButton.textContent = "delete"

       span.textContent = todo.text;
        li.appendChild(span)
        li.append(deleteButton)

       li.addEventListener("click",()=>{
            toggleCompelte(todo);
       })
       if(todo.isCompleted){
        li.classList.add("completed")
       }
       todoList.appendChild(li);   
}

//Init
{
    todo: [] 
}

//First 
{
    todo: [
        {text:"text"}
    ]
}
//First 
{
    todo: [
        {text:"text"},
        {text:"text 2 "}
    ]
}