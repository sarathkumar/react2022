import TodoFooter from "./components/todo_footer";
import TodoHeader from "./components/todo_header";
import TodoList from "./components/todo_list";

function App() {
  return (
    <div>
      <section className="todoapp">
        <TodoHeader/>
        <TodoList></TodoList>
        <TodoFooter/>
      </section>
    </div>
  );
}

export default App;
