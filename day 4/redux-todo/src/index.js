import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import { Provider } from "react-redux";
import App from './App';
import { store } from './store/store';

const root = ReactDOM.createRoot(document.getElementById('root'));


root.render(
  <Provider store={store} >
      <React.StrictMode>
        <App />
      </React.StrictMode>
  </Provider>
);


// import { legacy_createStore as createStore } from 'redux'

// function counterReducer(state = { value: 0 }, action) {
//   switch(action.type){
//     case 'incremented':{
//         return { ...state, value: state.value + 1 };
//     }
//     case 'decremented':{
//       return { ...state, value: state.value - 1 };
//     }        
//     default:{
//       return state;
//     }
//   }
// }

// let store = createStore(counterReducer)

// store.subscribe(() =>  {
//   console.log("Subs called",store.getState())
// })

// store.dispatch({ type: 'incremented' })
// // {value: 1}
// store.dispatch({ type: 'incremented' })
// store.dispatch({ type: 'incremented' })
// store.dispatch({ type: 'incremented' })
// store.dispatch({ type: 'incremented' })
// store.dispatch({ type: 'incremented' })
// // {value: 2}
 

// setTimeout(() => {
//     store.dispatch({ type: 'decremented' })
// }, 2000);