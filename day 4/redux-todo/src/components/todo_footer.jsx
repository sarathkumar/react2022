
import { clearCompleted, setFilter } from "../store/todo_store"; 

import { useSelector,useDispatch } from "react-redux";

const  TodoFooter = () => {
    return (
      <footer className="footer" style={{ display: "block" }}>
        <ItemsLeft />
        <TodoFilter />
        <button
          className="clear-completed"
          onClick={clearCompleted}
          style={{ display: "block" }}
        >
          Clear completed
        </button>
      </footer>
    );
}


const ItemsLeft = () => {

  const itemsLeft = useSelector((store)=>{
    return store.todoStore.todos.filter(i=>!i.isChecked).length
  }) 
  console.log("ItemsLeft re-rendering....");

  return (
    <span className="todo-count">
      <strong>{itemsLeft}</strong> items left
    </span>
  );
}
  

const TodoFilter = () => {

  const filter = useSelector((store) => store.todoStore.filter); 
  const dispatcher = useDispatch();
    console.log("TodoFilter re-rendering....");
    return (
      <ul className="filters">
        <li>
          <a
            href="#/"
            onClick={() => {
                dispatcher(setFilter(0))
            }}
            className={filter == 0 ? "active" : ""}
          >
            All
          </a>
        </li>
        <li>
          <a
            href="#/active"
            onClick={() => {
              dispatcher(setFilter(1))

            }}
            className={filter == 1 ? "active" : ""}
          >
            Active
          </a>
        </li>
        <li>
          <a
            href="#/completed"
            onClick={() => {
              dispatcher(setFilter(2))
            }}
            className={filter == 2 ? "active" : ""}
          >
            Completed
          </a>
        </li>
      </ul>
    );
  };
  
export default TodoFooter