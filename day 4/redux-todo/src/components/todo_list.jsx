import { useSelector, useDispatch } from 'react-redux'

import { toggleTodo,deleteTodo } from '../store/todo_store'

const TodoList = (props) => {
  
    const dispatch = useDispatch()
    const todos = useSelector((store)=> store.todoStore.todos)
    const filter = useSelector((store)=> store.todoStore.filter)

    let filteredTodos = todos;
    if(filter == 1){
      filteredTodos = todos.filter(i=>!i.isChecked)
    }
    if(filter == 2){
      filteredTodos = todos.filter(i=>i.isChecked)
    }

    return (
      <section className="main" style={{"display":"block"}}>
        <input id="toggle-all" className="toggle-all" type="checkbox" />
        <label for="toggle-all">Mark all as complete</label>
        <ul className="todo-list">
            {  filteredTodos.map(todo=>{

                    return (
                      <li
                        key={todo.id}
                        className={todo.isChecked ? "completed" : ""}
                      >
                        <div className="view">
                          <input
                            className="toggle"
                            type="checkbox"
                            onChange={() => {
                            //   props.onCheck(todo.id);
                              dispatch(toggleTodo(todo.id));
                            }}
                            checked={todo.isChecked}
                          />
                          <label>{todo.text}</label>
                          <button
                            className="destroy"
                            // onClick={() => props.onDelete(todo.id)}
                            onClick={()=>{
                                dispatch(deleteTodo(todo.id));
                            }}
                          ></button>
                        </div>
                      </li>
                    );

            })
            }
         
        </ul>
      </section>
    );
}
 
export default TodoList