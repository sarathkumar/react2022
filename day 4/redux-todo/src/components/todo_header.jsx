import { useState,useRef } from "react";
import { addTodo } from "../store/todo_store";
import { useSelector, useDispatch } from 'react-redux'

const TodoHeader = (props) => {
    const todoInput = useRef()
    const dispatch = useDispatch()

    return (
      <header className="header">
        <h1>Todos</h1>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            const todo = {
              text: todoInput.current.value,
              isChecked: false,
              id: new Date().getTime(),
            };
            todoInput.current.value = ""
            dispatch(addTodo(todo))
          }}
        >
          <input
            ref={todoInput}
            className="new-todo"
            placeholder="What needs to be done?"
          />
        </form>
      </header>
    );
}
 


export default TodoHeader