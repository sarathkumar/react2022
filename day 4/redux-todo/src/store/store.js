import { configureStore,createSlice } from '@reduxjs/toolkit'
import {reducer} from './todo_store'


export const store = configureStore({
    reducer: {
      todoStore:  reducer,
    },
  }); 
  
   