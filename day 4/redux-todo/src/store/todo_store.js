import {  createSlice } from '@reduxjs/toolkit'

// todo/addTodo

const initialState = {
  todos: [],
  filter: 0,
};

export const todoSlice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    addTodo: (state, action) => {
      state.todos = [...state.todos, action.payload];
    },
    toggleTodo: (state, { payload }) => {
      state.todos = [
        ...state.todos.map((element) => {
          if (element.id == payload) {
            element.isChecked = !element.isChecked;
          }
          return element;
        }),
      ];
    },
    deleteTodo: (state, { payload }) => {
      state.todos = [...state.todos.filter((e) => e.id != payload)];
    },
    setFilter(state, { payload }) {
      state.filter = payload;
    },
    clearCompleted(state, { payload }) {
      state.todos = [...state.todos.filter((e) => !e.isChecked)];
    },
  },
});
 

export const reducer = todoSlice.reducer
export const { addTodo, toggleTodo, deleteTodo, clearCompleted, setFilter } =
  todoSlice.actions;