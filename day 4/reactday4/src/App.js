import { useEffect, useState } from "react";
import TodoApp from "./components/todo_app";

// USING HOC

// function App() {
//   return (
//     <div>
//       <p>Hello</p>
//       <ShortCutWrapper
//         component={ShortCutPrinter}
//         keyCode="a"
//         style={{ color: "red" }}
//       />
//       <ShortCutWrapper
//         component={ColorComponent}
//         keyCode="b"
//         message="Hello Gadgeon"
//       />
//     </div>
//   );
// }

// function ShortCutPrinter({ style, keyPressed }) {
//   return (
//     <div style={style}>
//       <p>ShortCut Pressed : { keyPressed.toString()} </p>
//     </div>
//   );
// }

// function ColorComponent(props){
//   return (
//     <div>
//       <p style={{ backgroundColor: props.keyPressed ? "green" : "white" }}>
//         {props.message}
//       </p>
//     </div>
//   );
// }


// function ShortCutWrapper(props){
//   const [keyPressed,setKeyState] = useState(false)
  
//   useEffect(() => { 
//     const listener = (event) => {
//       let key = false;
//       if (event.ctrlKey) {
//         if (event.key === props.keyCode) {
//           key = true;
//           console.log("value set");
//         }
//       }
//       setKeyState(key);
//     };
//     window.addEventListener("keydown", listener);
//   }, []);

//   return props.component({ ...props, keyPressed });


// }


// USING HOOKS

// function App() {
//   return (
//     <div>
//       <p>Hello</p>
//       <ShortCutPrinter />
//       <ColorComponent message="Hello gadgeon"/>
//       {/* <ShortCutWrapper
//         component={ColorComponent}
//         keyCode="b"
//         message="Hello Gadgeon"
//       /> */}
//     </div>
//   );
// }

// function ShortCutPrinter({ style }) {
//   const keyPressed = useKeyboardShortCut("b")
//   return (
//     <div style={style}>
//       <p>ShortCut Pressed : {keyPressed.toString()} </p>
//     </div>
//   );
// }

// function ColorComponent(props){
//   const keyPressed = useKeyboardShortCut("a")
//   return (
//     <div>
//       <p style={{ backgroundColor: keyPressed ? "green" : "white" }}>
//         {props.message}
//       </p>
//     </div>
//   );
// }

 
// function useKeyboardShortCut(keyCode){
//   const [keyPressed,setKeyState] = useState(false)
 
//   useEffect(() => {
//     const listener = (event) => {
//       let key = false;
//       if (event.ctrlKey) {
//         if (event.key === keyCode) {
//           key = true;
//         }
//       }
//       setKeyState(key);
//     };
//     window.addEventListener("keydown", listener);
//     return () => window.removeEventListener("keydown", listener);
//   }, []);
  
//   return keyPressed;
// }



function App(){
    return <TodoApp/>
}



export default App;
