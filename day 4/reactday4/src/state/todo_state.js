import { createContext, useState, useEffect, useContext } from "react";

const TodoStoreContext = createContext();

function useTodoState() {
  const [todos, setTodo] = useState([]);
  const [filter, setFilter] = useState(0);

  const state = {
    todos,
    filter,
    setFilter,
    addTodo: (todo) => {
      setTodo([...todos, todo]);
      console.log("called");
    },
    toggleTodo: (id) => {
        setTodo([
            ...todos.map((element) => {
              if (element.id == id) {
                element.isChecked = !element.isChecked;
              }
              return element;
            }),
          ]);
    },
    clearCompleted:()=>{
        setTodo([...todos.filter((e) => !e.isChecked)]);
    },
    deleteTodo: (id) => {
        setTodo([...todos.filter((e) => e.id != id)]);
    },
  };

  return state;
}

export const TodoProvider = (props) => {
  const todoState = useTodoState();
  return (
    <TodoStoreContext.Provider value={todoState}>
      {props.children}
    </TodoStoreContext.Provider>
  );
};

export const useTodo = () => useContext(TodoStoreContext);
