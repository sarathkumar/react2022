import { TodoProvider, useTodo } from "../state/todo_state";
import {useContext, useState} from 'react'
import TodoHeader from "./todo_header";
import TodoList from "./todo_list";
import TodoFooter from "./todo_footer";

const TodoApp = () => {
    return (
      <TodoProvider>
        <section className="todoapp">
          <TodoHeader />
          <TodoList />
          <TodoFooter />
        </section>
      </TodoProvider>
    );
}
 
export default TodoApp;