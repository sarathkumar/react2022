import { useTodo } from "../state/todo_state";

const  TodoFooter = () => {

    const {todos,clearCompleted} = useTodo()

    const itemsLeft = todos.filter(i=>!i.isChecked).length
    return (
      <footer className="footer" style={{ display: "block" }}>
        <span className="todo-count">
          <strong>{itemsLeft}</strong> items left
        </span>
        <TodoFilter  />
        <button className="clear-completed" 
        onClick={clearCompleted}
        style={{ display: "block" }}>
          Clear completed
        </button>
      </footer>
    );
}



const TodoFilter = () => {

    const {filter,setFilter} = useTodo()
    console.log("TodoFilter re-rendering....")
    return (
      <ul className="filters">
        <li>
          <a
            href="#/"
            onClick={() => {
                setFilter(0);
            }}
            className={filter == 0 ? "active" : ""}
          >
            All
          </a>
        </li>
        <li>
          <a
            href="#/active"
            onClick={() => {
                setFilter(1);
            }}
            className={filter == 1 ? "active" : ""}
          >
            Active
          </a>
        </li>
        <li>
          <a
            href="#/completed"
            onClick={() => {
                setFilter(2);
            }}
            className={filter == 2 ? "active" : ""}
          >
            Completed
          </a>
        </li>
      </ul>
    );
  };
  
export default TodoFooter