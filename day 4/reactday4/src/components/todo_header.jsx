import { useState,useRef } from "react";
import { useTodo } from "../state/todo_state";

const TodoHeader = (props) => {
    const {addTodo} = useTodo();
    const todoInput = useRef()
    return (
      <header className="header">
        <h1>Todos</h1>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            const todo = {
              text: todoInput.current.value,
              isChecked: false,
              id: new Date().getTime(),
            };
            todoInput.current.value = ""
            addTodo(todo);
          }}
        >
          <input
            ref={todoInput}
            className="new-todo"
            placeholder="What needs to be done?"
          />
        </form>
      </header>
    );
}
 


export default TodoHeader