import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import {
  Routes,
  Route,
  useParams,
  Link
} from "react-router-dom";
import WeatherApp from './components/weather_app';

function App() {
  const [page,setPage] = useState(0)
  return (
    <div className="App">
      <Link to="/">Home</Link>
      <Link to="/about">About</Link>
      <Link to="/login">Login</Link>
      <Link to="/user/jon">User</Link>
      <Link to="/weather">Weather </Link>
      <div>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/about" element={<About />} />
          <Route exact path="/login" element={<Login />} />
          <Route exact path="/user/:name" element={<User />} />
          <Route exact path="/weather" element={<WeatherApp />} />
        </Routes>
      </div>
    </div>
  );
}
const User = ()=>{
  let { name } = useParams();
  return (
    <div style={{ backgroundColor: "grey" }}>
      <h4>User : {name}</h4>
    </div>
  );
}


const About = () => {
  return (
    <div style={{ backgroundColor: "aqua" }}>
      <h4>About page</h4>
    </div>
  );  
 
}
 
const Login = () => {
  return  ( <div style={{backgroundColor: "yellow"}}>
  <h4>Login page</h4>
  <input></input>
  <input></input>
  <button>Submit</button>
</div>)
 
}
 
const Home = () => {
  return (
    <div style={{ backgroundColor: "green" }}>
      <h4>Welcome to the react app</h4>
    </div>
  ); 
  
}
 
 export default App;
