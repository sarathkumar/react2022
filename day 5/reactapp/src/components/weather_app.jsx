import { useEffect, useState } from 'react';
import './style.css'

const WeatherApp = ()=>{
    const [weather,setWeather] = useState({})
    const [city,setCity ] = useState("newyork")
    const [loading,setLoading ] = useState(false)
    const [error,setError ] = useState(null)

    
    useEffect(() => {
      setError(null)
      setLoading(true)
      fetch(`https://goweather.herokuapp.com/weather/${city}`)
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw "Not found";
          }
        })
        .then((value) => {
          setWeather(value);
          setLoading(false)
        })
        .catch((error) => {
          console.log("ERROR ");
          setError(error);
          setLoading(false)
        });
    }, [city]);


    return (
      <div>
        <h4>Weather App</h4>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            setCity(e.target[0].value);
          }}
        >
          <input type="text"></input>
        </form>
        <div className="weathercard">
          <h2> City : {city}</h2>
          {error == null && !loading && (
            <div>
              <h3> {weather.description} </h3>
              <h4>
                {" "}
                {weather.temperature} | {weather.wind}{" "}
              </h4>
            </div>
          )}
          {error != null && <p style={{ color: "red" }}>Error API : {error}</p>}
          {loading && <p style={{ color: "blue" }}>Loading....</p>}
          <div></div>
        </div>
      </div>
    );
}

export default WeatherApp;