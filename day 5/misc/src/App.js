import logo from './logo.svg';
import './App.css';
import {motion} from 'framer-motion'
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <motion.div
           animate={{
            scale: [1, 2, 2, 1, 1],
            rotate: [0, 0, 270, 270, 0],
            borderRadius: ["20%", "20%", "50%", "50%", "100%"],
          }}
          transition={{
            duration: 2,
            type: "spring",
            times: [0, 0.2, 0.5, 0.8, 1],
            repeat: Infinity,
            repeatDelay: 1
          }}
          className="box"
        >
          <p>Hello</p>

        </motion.div>
      </header>
    </div>
  );
}

export default App;
