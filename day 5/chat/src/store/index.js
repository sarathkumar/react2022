import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./user";
import messageReducer from "./messages";
export const store = configureStore({
  reducer: {
    user: userReducer,
    message: messageReducer,
  },
});
