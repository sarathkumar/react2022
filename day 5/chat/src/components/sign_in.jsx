import { Button, Grid, Box } from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import FormControl, { useFormControl } from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import LayoutBase from "./layout_base";
import { useNavigate } from "react-router-dom";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { useState } from "react";

export default function SignIn() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);

  const handleSubmit = (event) => {
    console.log(event);
    event.preventDefault();
    const auth = getAuth();
    const email = event.target[0].value;
    const password = event.target[2].value;
    setLoading(true);
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        setLoading(false);
        navigate("/chat");
      })
      .catch((error) => {
        setLoading(false);
        const errorCode = error.code;
        const errorMessage = error.message;
        // ..
      });
  };
  return (
      <Grid
        container
        align="center"
        justify="center"
        direction="row"
        component="main"
        sx={{ height: "94vh" }}
      >
        <Grid item sm={4} md={4}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              padding: 6,
            }}
          >
            <Box
              component="form"
              onSubmit={handleSubmit}
              noValidate
              sx={{ mt: 24 }}
            >
              <h2>Log into your account</h2>
              <FormControl sx={{ mt: 0 }} required fullWidth>
                <OutlinedInput placeholder="Email" type="email" />
              </FormControl>
              <FormControl sx={{ mt: 4 }} required fullWidth>
                <OutlinedInput placeholder="Password" type="password" />
              </FormControl>
              <LoadingButton
                type="submit"
                fullWidth
                loading={loading}
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Sign In
              </LoadingButton>

              <Grid>
                <Grid>
                  <p>No account?</p>
                </Grid>
                <Grid>
                  <Button
                    onClick={() => {
                      navigate("/signup");
                    }}
                  >
                    Create one
                  </Button>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
        <Grid
          item
          xs={false}
          sm={8}
          md={8}
          sx={{
            backgroundImage:
              "url(https://source.unsplash.com/random/?city,night,nature,animals)",
            backgroundRepeat: "no-repeat",
            width: "100%",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
      </Grid>
  );
}
