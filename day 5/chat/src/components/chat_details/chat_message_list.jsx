import {
  List,
} from "@mui/material";
import ImageIcon from "@mui/icons-material/Person";
import MessageBubble from "./message_bubble";
import { useParams } from "react-router-dom";
import { useEffect } from "react";
import { subscribeToRoom } from "../../firebase/database";
import { getAuth } from "firebase/auth";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";

export default function ChatMessageList() {
  const { chatid } = useParams();
  const dispatch = useDispatch();
  const messages = useSelector((state) => state.message.messages);
  useEffect(() => {
    const auth = getAuth();
    subscribeToRoom(auth.currentUser.uid, chatid, dispatch);
  }, [chatid]);
  return (
    <List
      sx={{
        width: "100%",
        height: "88%",
        position: "relative",
        overflow: "auto",
      }}
      cols={3}
      rowHeight={164}
    >
      {messages.map((item) => (
        <MessageBubble
          message={item}
          isRight={item.uid !== chatid}
        ></MessageBubble>
      ))}
    </List>
  );
}
