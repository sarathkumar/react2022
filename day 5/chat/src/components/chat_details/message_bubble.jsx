import {
  Container,
  Link,
  List,
  ListItem,
  ListItemText,
  Button,
  Chip,
  TextField,
  Avatar,
  Stack,
  ListItemAvatar,
  Typography,
  Grid,
  Paper,
  Box,
  Divider,
} from "@mui/material";
import { padding, textAlign } from "@mui/system";

export default function MessageBubble({ message, isRight = false, children }) {
  console.log(isRight);
  return (
    <div
      style={{
        display: "flex",
        alignItems: isRight ? "flex-end" : "flex-start",
        flexDirection: "column",
        padding: 12,
      }}
    >
      <Box
        sx={{ boxShadow: 1 }}
        style={{
          maxWidth: 360,
          background: isRight ? "#537CFA" : "#F9F9F9",
          padding: 8,
          color: isRight ? "white" : "black",
          borderRadius: 8,
          borderBottomRightRadius: isRight ? 0 : 8,
          borderBottomLeftRadius: isRight ? 8 : 0,
        }}
      >
        <Box>
          <ListItemText
            dense
            sx={{
                display: "flex",
                flexWrap: "nowrap",
                alignItems: "flex-start",
                flexDirection: "column",
                alignContent: "center",
                textAlign:"start",
                paddingLeft: isRight ? 2:0
            }}
            inset={true}
            primary={message.text}
            secondary={
              <Typography
                sx={{ color: isRight ? "#d2d2d2" : "#959595" }}
                variant="caption"
              >
                Jan 9, 2021
              </Typography>
            }
          ></ListItemText>
        </Box>
      </Box>
    </div>
  );

  
}
 