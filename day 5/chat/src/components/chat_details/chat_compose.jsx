import {
  Container,
  Link,
  List,
  ListItem,
  ListItemText,
  Button,
  Fab,
  Chip,
  ListItemIcon,
  TextField,
  Avatar,
  Stack,
  ListItemAvatar,
  Typography,
  Grid,
  Paper,
  Box,
  Divider,
} from "@mui/material";
import SendIcon from "@mui/icons-material/Send";
import LoadingButton from "@mui/lab/LoadingButton";
import FormControl, { useFormControl } from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import { useParams } from "react-router-dom";
import { addNewMessage } from "../../firebase/database";
import { getAuth } from "firebase/auth";
import { useState } from "react";

export default function ChatCompose() {
  const { chatid } = useParams();
  const [message, setMessage] = useState();

  const onSubmit = (ev) => {
    console.log("HELLO");
    if (ev.key === "Enter") {
      ev.preventDefault();
      const { currentUser } = getAuth();
      addNewMessage(
        {
          text: message,
          time: Date.now(),
        },
        currentUser.uid,
        chatid
      );
      setMessage("");
    }
  };
  return (
    <ListItem onSubmit={onSubmit} component="form" noValidate>
      <FormControl sx={{ mt: 0 }} required fullWidth>
        <OutlinedInput
          onKeyPress={onSubmit}
          onChange={(event) => {
            setMessage(event.target.value);
          }}
          value={message}
          style={{ borderRadius: 34 }}
          multiline
          placeholder="Name"
        />
      </FormControl>
      <Fab color="primary">
        <SendIcon sx={{ mr: 1, ml: 1 }} />
      </Fab>
    </ListItem>
  );
}
