import { Paper, Container, Grid } from "@mui/material";

export default function LayoutBase({ children }) {
  return (
    <Container maxWidth="xl">
      <Grid
        direction="column"
        container
        justifyContent="center"
      >
        <Grid xs={12} sm={12} md={12} style={{ height: "98vh",margin:12 }}>
          <Paper  square={false} style={{ height: "94vh"}} elevation={8}>
            {children}
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
}
