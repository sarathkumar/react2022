import {
  Container,
  Link,
  List,
  ListItem,
  ListItemText,
  Button,
  Chip,
  TextField,
  Avatar,
  Stack,
  ListItemAvatar,
  Typography,
  Grid,
  Paper,
  Box,
  Divider,
  Skeleton,
} from "@mui/material";
import { Outlet, useParams } from "react-router-dom";
import ImageIcon from "@mui/icons-material/Person";
import ChatSideBar from "./chat_sidebar";
import ChatCompose from "./chat_details/chat_compose";
import LayoutBase from "./layout_base";
import ChatDetail from "./chat_details";

export default function ChatDashboard(props) {
  return (
    <Grid
      container
      align="center"
      justify="center"
      direction="row"
      component="main"
      sx={{ height: "94vh" }}
    >
      <Grid
        item
        sm={3}
        md={3}
        sx={{
          height: "100%",
          backgroundColor: () => "#F6F7FC",
        }}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            height: "100%",
          }}
        >
          <ChatSideBar></ChatSideBar>
        </Box>
      </Grid>
      <Grid
        item
        xs={false}
        sm={9}
        md={9}
        sx={{
          height: "100%",

          width: "100%",
        }}
      >
        <Outlet></Outlet>
      </Grid>
    </Grid>
  );
}
