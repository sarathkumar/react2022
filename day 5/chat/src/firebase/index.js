// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD5pg94K2M-PEZbUFtxkUGbqh3PIw3hH9o",
  authDomain: "chatapp-f576c.firebaseapp.com",
  databaseURL: "https://chatapp-f576c-default-rtdb.firebaseio.com",
  projectId: "chatapp-f576c",
  storageBucket: "chatapp-f576c.appspot.com",
  messagingSenderId: "595986641549",
  appId: "1:595986641549:web:32d5bc1370d35f93fb68c4",
  measurementId: "G-HV0MWBN3MJ"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);

const database = getDatabase();

export { app, database };
