import { ref, onValue, push, set } from "firebase/database";
import { addMessage } from "../store/messages";
import { setChatUsers } from "../store/user";
import { database } from "./index";

const userRef = ref(database, "users");

export function findUser(userid) {
  return new Promise((res, rej) => {
    onValue(
      userRef,
      (snapshot) => {
        let foundUser = null;
        snapshot.forEach((childSnapshot) => {
          if (childSnapshot.val().uid == userid) {
            foundUser = childSnapshot.val();
          }
        });
        if (foundUser != null) {
          res(foundUser);
        } else {
          rej("No user found");
        }
      },
      {
        onlyOnce: true,
      }
    );
  });
}

export function subscribeToUsers(dispatch, userid) {
  onValue(
    userRef,
    (snapshot) => {
      let chatUsers = [];
      snapshot.forEach((childSnapshot) => {
        if (childSnapshot.val().uid != userid) {
          chatUsers.push(childSnapshot.val());
        }
      });
      dispatch(setChatUsers(chatUsers));
    },
    {
      onlyOnce: false,
    }
  );
}

export function addNewMessage(message, userid, chatUserId) {
  const roomId = [userid,chatUserId].sort().join('-')
  const messageRef = ref(database, "messages/" + `${roomId}`);
  const newMessageRef = push(messageRef);
  set(newMessageRef, {
    ...message,
    uid: userid,
  });
}
export function subscribeToRoom(userid, chatUserId, dispatch) {
  const roomId = [userid,chatUserId].sort().join('-')
  const messageRef = ref(database, "messages/" + `${roomId}`);
  onValue(
    messageRef,
    (snapshot) => {
      const messages = [];
      snapshot.forEach((childSnapshot) => {
        messages.push(childSnapshot.val());
      });
      dispatch(addMessage(messages));
    },
    {
      onlyOnce: false,
    }
  );
}
